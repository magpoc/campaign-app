﻿using System;
using System.Collections.Generic;
using System.Linq;
using Campaign.WebApp.Models.Customers;

namespace Campaign.WebApp.Infrastructure
{
    internal sealed class CustomerDataSource : ICustomerRepository
    {
        private readonly ApplicationDbContext _db;

        public CustomerDataSource(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Customer> All()
        {
            return _db.Customers.AsEnumerable();
        }

        public Customer Find(Guid customerId)
        {
            return _db.Customers.Find(customerId);
        }
    }
}