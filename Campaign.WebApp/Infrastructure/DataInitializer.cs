﻿using System;
using System.IO;
using System.Linq;
using Campaign.WebApp.Models;
using Campaign.WebApp.Models.Customers;
using CsvHelper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Campaign.WebApp.Infrastructure
{
    internal static class DataInitializer
    {
        public static void ImportInitialData(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                db.ImportCustomers("Infrastructure/customers.csv");
            }
        }

        private static void ImportCustomers(this ApplicationDbContext db, string filename)
        {
            using (var reader = new StreamReader(filename))
            using (var csv = new CsvReader(reader))
            {
                var data = csv.GetRecords<CustomerRow>()
                    .Select(row => row.Build());

                db.AddRange(data);
                db.SaveChanges();
            }
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        // ReSharper disable MemberCanBePrivate.Local
        // ReSharper disable UnusedAutoPropertyAccessor.Local
        // ReSharper disable UnusedMember.Global
        private sealed class CustomerRow
        {
            public string 名前 { get; set; }
            public string ふりがな { get; set; }
            public string 性別 { get; set; }
            public int 年齢 { get; set; }
            public DateTime 誕生日 { get; set; }

            public Customer Build()
            {
                return new Customer(Guid.NewGuid(), 名前, Gender(性別), new BirthDate(誕生日));
            }

            private static Gender Gender(string label)
            {
                if (label == "男") return Models.Customers.Gender.Male;
                if (label == "女") return Models.Customers.Gender.Female;

                throw new ArgumentException();
            }
        }
    }
}