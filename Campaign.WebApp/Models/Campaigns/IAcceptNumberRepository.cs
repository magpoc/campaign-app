﻿namespace Campaign.WebApp.Models.Campaigns
{
    public interface IAcceptNumberRepository
    {
        int FindPrevious();
        void SavePrevious(int value);
    }
}