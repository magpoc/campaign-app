﻿using System.Globalization;
using Campaign.WebApp.Models.Customers;

namespace Campaign.WebApp.Models.Campaigns
{
    internal sealed class ApplyCampaignService : IApplyCampaignService
    {
        private readonly AcceptNumberGenerator _acceptNumberGenerator;
        private readonly ICampaignApplicationRepository _applications;

        public ApplyCampaignService(AcceptNumberGenerator acceptNumberGenerator,
            ICampaignApplicationRepository applications)
        {
            _acceptNumberGenerator = acceptNumberGenerator;
            _applications = applications;
        }

        public int Apply(Customer customer)
        {
            if (!Applicable(customer)) return 0;

            var acceptNumber = _acceptNumberGenerator.NewNumber();

            var application = new CampaignApplication(acceptNumber, customer.Id);

            _applications.Add(application);

            return acceptNumber;
        }

        private bool Applicable(Customer customer)
        {
            if (customer.Gender != Gender.Male) return false;
            var cultureJp = new CultureInfo("ja-JP")
            {
                DateTimeFormat = {Calendar = new JapaneseCalendar()}
            };
            if (customer.BirthDate.ToDateTime().ToString("gg", cultureJp) != "平成") return false;

            return true;
        }
    }
}