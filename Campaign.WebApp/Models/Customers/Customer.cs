﻿using System;

namespace Campaign.WebApp.Models.Customers
{
    public sealed class Customer
    {
        public Customer(Guid id, string name, Gender gender, BirthDate birthDate)
        {
            Id = id;
            Name = name;
            Gender = gender;
            BirthDate = birthDate;
        }

        // ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public Gender Gender { get; private set; }
        public BirthDate BirthDate { get; private set; }
    }
}